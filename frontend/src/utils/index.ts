export const handleNumberPad = (inputNumber: number) => {
    return inputNumber.toString().padStart(2, '0');
};