import React from 'react';
import {zonedTimeToUtc} from 'date-fns-tz';
import { Autocomplete, Button, Grid, TextField } from '@mui/material';
import Timezone from './components/Timezone';
import timezones from './constants/Timezones';

function App() {
  const [utcDate, setUtcDate] = React.useState<Date>(new Date());
  const [currentTimezones, setCurrentTimezones] = React.useState<string[]>(['GMT', Intl.DateTimeFormat().resolvedOptions().timeZone]);
  const [value, setValue] = React.useState<string | null>(timezones[0]);
  const [inputValue, setInputValue] = React.useState<string>('');

  const handleDateChange = (input: string, timezone: string) => {
    const [hours, minutes, seconds] = input.split(":").map(Number);
    const targetDate = new Date(utcDate.getFullYear(), utcDate.getMonth(), utcDate.getDay(), hours, minutes, seconds);
    setUtcDate(zonedTimeToUtc(targetDate, timezone));
  };

  const handleValueChange = (_: any, newValue: string | null) => {
    setValue(newValue);
  };

  const handleAddTimezone = () => {
    setCurrentTimezones((oldValue) => {
      oldValue.push(value as string);
      return [...oldValue];
    });
  };

  return (
    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
      <h2>Timezone Converter</h2>
      <Grid container alignItems={'center'} justifyContent='space-between' style={{width: '38rem', marginBottom: '1rem'}}>
        <Grid item>
          <Autocomplete inputValue={inputValue} value={value} options={timezones} renderInput={(params) => <TextField {...params} label="Select a timezone" />} sx={{width: 300}} onInputChange={(_, newInputValue) => {setInputValue(newInputValue)}} onChange={handleValueChange} />
        </Grid>
        <Grid item>
          <Button disabled={value === null} variant='contained' onClick={handleAddTimezone}>ADD</Button>
        </Grid>
      </Grid>
      {currentTimezones.map((timezone) => <Timezone key={timezone} currentDate={utcDate} name={timezone} onTimeChange={handleDateChange} />)}
    </div>
  );
}

export default App;
