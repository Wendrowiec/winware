import React from 'react';
import {utcToZonedTime} from 'date-fns-tz';
import {handleNumberPad} from '../utils';

interface ClockProps {
    date: Date;
    timezone: string;
}

const Clock: React.FC<ClockProps> = ({date, timezone}) => {
    const [currentDate, setCurrentDate] = React.useState<Date>(utcToZonedTime(date, timezone));

    React.useEffect(() => {
        const interval = setInterval(() => 
            setCurrentDate((oldDate) => {
                return new Date(
                    oldDate.getFullYear(),
                    oldDate.getMonth(),
                    oldDate.getDay(),
                    oldDate.getHours(),
                    oldDate.getMinutes(),
                    oldDate.getSeconds() + 1
                  );
        }), 1000);
        return () => clearInterval(interval);
    }, []);

    React.useEffect(() => {
        setCurrentDate(utcToZonedTime(date, timezone));
    }, [date]);

    return (
        <p style={{fontWeight: 700, fontSize: '20px', lineHeight: '24px'}}>{handleNumberPad(currentDate.getHours())}:{handleNumberPad(currentDate.getMinutes())}:{handleNumberPad(currentDate.getSeconds())}</p>
    );
};

export default Clock;
