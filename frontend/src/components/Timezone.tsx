import React from "react";
import {TextField, Button, Paper, Grid} from '@mui/material';
import Clock from './Clock'

interface TimezoneProps {
    currentDate: Date;
    name: string;
    onTimeChange: (input: string, timezone: string) => void;
};

const timeValidator = /(?:[01]\d|2[0-3]):(?:[0-5]\d):(?:[0-5]\d)/;

const Timezone: React.FC<TimezoneProps> = ({currentDate, name, onTimeChange}) => {
    const [newTime, setNewTime] = React.useState<string>('');

    const handleTimeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewTime(event.target.value);
    };

    const handleSubmitTime = () => {
        newTime && onTimeChange(newTime, name);
    };

    const isInputInvalid = () => !timeValidator.test(newTime);

    return(
        <Paper style={{padding: '2rem', width: '34rem', marginBottom: '1rem'}}>
            <Grid container alignItems={'center'} justifyContent='space-between' gap={1}>
                <Grid item>
                    <Grid container direction={'column'}>
                        <Grid item style={{marginRight: '1rem'}}>
                            <Clock date={currentDate} timezone={name} />
                        </Grid>
                        <h3>{name}</h3>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid direction='column' container justifyContent={'center'} alignItems='center' gap={1}>
                        <Grid item>
                            <TextField data-testid={`timezone-input-${name}`} name={`timezone-input-${name}`} id={`timezone-input-${name}`} type={'text'} label="Time" placeholder="16:00:00" value={newTime} onChange={handleTimeChange} error={Boolean(newTime) && isInputInvalid()} helperText={isInputInvalid() ? "Please provide time in the following format 16:00:00": ' '} style={{width: '21rem'}} />
                        </Grid>
                        <Grid item>
                            <Button variant="contained" disabled={isInputInvalid()} onClick={handleSubmitTime}>SUBMIT</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default Timezone;
