import React from 'react';
import {act, render, screen} from '@testing-library/react';
import Clock from '../components/Clock';
import { handleNumberPad } from '../utils';

test('renders time properly', () => {
    const currentDate = new Date();
    render(<Clock date={currentDate} timezone={Intl.DateTimeFormat().resolvedOptions().timeZone} />);
    const time = screen.getByText(`${handleNumberPad(currentDate.getHours())}:${handleNumberPad(currentDate.getMinutes())}:${handleNumberPad(currentDate.getSeconds())}`);
    expect(time).toBeInTheDocument();
});

test('time ticks properly', () => {
    jest.useFakeTimers();
    const currentDate = new Date();
    render(<Clock date={currentDate} timezone={Intl.DateTimeFormat().resolvedOptions().timeZone} />);
    act(() => {
        jest.advanceTimersByTime(1000);
    });
    currentDate.setSeconds(currentDate.getSeconds() + 1);
    const time = screen.getByText(`${handleNumberPad(currentDate.getHours())}:${handleNumberPad(currentDate.getMinutes())}:${handleNumberPad(currentDate.getSeconds())}`);
    expect(time).toBeInTheDocument();
});
