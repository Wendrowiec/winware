import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import App from '../App';
import timezones from '../constants/Timezones';

test('renders app title properly', () => {
  render(<App />);
  const titleElement = screen.getByText(/Timezone Converter/i);
  expect(titleElement).toBeInTheDocument();
});

test('renders initial timezones properly', () => {
  render(<App />);
  const gmtTimezone = screen.getByText(/GMT/i);
  expect(gmtTimezone).toBeInTheDocument();
  const userTimezone = screen.getByText(Intl.DateTimeFormat().resolvedOptions().timeZone);
  expect(userTimezone).toBeInTheDocument();
});

test('adds timezone', () => {
  render(<App />);
  const addButton = screen.getByText(/ADD/i);
  fireEvent(addButton, new MouseEvent('click', {
    bubbles: true,
    cancelable: true,
  }));
  const timezone = screen.getByText(timezones[0]);
  expect(timezone).toBeInTheDocument();
});

test('time change is handled properly', () => {
  render(<App />);
  const [textField] = screen.getAllByTestId('timezone-input-GMT');
  // eslint-disable-next-line testing-library/no-node-access
  const timeInput = textField.querySelector('input');
  expect(timeInput).toBeInTheDocument();
  fireEvent.change(timeInput as HTMLInputElement, {target: {value: '16:00:00'}});
  const [submitButton] = screen.getAllByText(/SUBMIT/i);
  fireEvent(submitButton, new MouseEvent('click', {
    bubbles: true,
    cancelable: true,
  }));
  const time = screen.getByText('16:00:00');
  expect(time).toBeInTheDocument();
});
