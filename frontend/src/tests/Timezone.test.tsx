import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import Timezone from '../components/Timezone';

test('display timezone properly', () => {
    render(<Timezone currentDate={new Date()} name="GMT" onTimeChange={(input: string, timezone: string) => {}} />);
    const timezone = screen.getByText("GMT");
    expect(timezone).toBeInTheDocument();
});

test('display error on wrong input format', () => {
    render(<Timezone currentDate={new Date()} name="GMT" onTimeChange={(input: string, timezone: string) => {}} />);
    // eslint-disable-next-line testing-library/no-node-access
    const timeInput = screen.getByTestId('timezone-input-GMT').querySelector('input');
    expect(timeInput).toBeInTheDocument();
    fireEvent.change(timeInput as HTMLInputElement, {target: {value: '16:0:0'}});
    const submitButton = screen.getByText(/SUBMIT/i);
    expect(submitButton).toBeDisabled();
    const errorMessage = screen.getByText('Please provide time in the following format 16:00:00');
    expect(errorMessage).toBeInTheDocument();
});
